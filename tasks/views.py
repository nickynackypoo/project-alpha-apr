from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Task
from .forms import TaskForm


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    return render(request, "show_my_tasks.html", {"tasks": tasks})


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(
                "projects_list"
            )  # Adjust the redirect URL as needed
    else:
        form = TaskForm()

    return render(request, "create_task.html", {"form": form})
