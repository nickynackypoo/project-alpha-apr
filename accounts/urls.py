from django.urls import path
from accounts.views import user_login, user_logout, signup
from projects.views import list_projects


urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("signup/", signup, name="signup"),
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
]
